#!/bin/sh
set -eu

INSTALL_PREFIX=$HOME/.local
INSTALL_PREFIX=$INSTALL_PREFIX DESKTOP_PREFIX=$INSTALL_PREFIX ./install.sh

# Open incoming ports
echo "Opening Cwtch firewall for incoming traffic on ports 15000 to 15378"
sudo mkdir -p /usr/local/etc/whonix_firewall.d
echo "EXTERNAL_OPEN_PORTS+=\" \$(seq 15000 15378) \"" | \
  sudo tee /usr/local/etc/whonix_firewall.d/40_cwtch.conf >/dev/null
sudo whonix_firewall

# Set launch options
sed -i "s|env LD|env CWTCH_TAILS=true CWTCH_RESTRICT_PORTS=true CWTCH_BIND_EXTERNAL_WHONIX=true LD|" $INSTALL_PREFIX/bin/cwtch

# Inform about steps to be done in the gateway
echo "Complete installation in the Whonix-Gateway with the following command:"
echo "  $ sudo onion-grater-add 40_cwtch"

echo "Launch Cwtch in the Whonix-Workstation with:"
echo "  $ $INSTALL_PREFIX/bin/cwtch"
