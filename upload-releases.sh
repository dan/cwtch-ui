#!/bin/bash

VERSION=$(cat VERSION)
echo "Grabbing Release Data From:"  "https://git.openprivacy.ca/api/v1/repos/$DRONE_REPO/releases/tags/$VERSION"
RELEASEID=$(curl -s -X 'GET' "https://git.openprivacy.ca/api/v1/repos/$DRONE_REPO/releases/tags/$VERSION" -H 'accept: application/json' | jq '.id')
echo $RELEASEID
if [ "$RELEASEID" = "null" ]; then
    # $var is empty
    exit 0
fi


URL="https://git.openprivacy.ca/api/v1/repos/$DRONE_REPO/releases/$RELEASEID/assets?name=$3"
FILE="@$1"
RESULT=$(curl -o /dev/null -w "%{http_code}" -X POST -H "Authorization: token $GOGS_ACCOUNT_TOKEN" -H "accept: application/json"  -H "Content-Type: multipart/form-data" -F "attachment=$FILE;type=$2" $URL)


if [ $RESULT -eq 201 ]
then
	echo "Success posting to $URL"
else
	echo "ERROR HTTP $RESULT posting to $URL"
	exit 0
fi
