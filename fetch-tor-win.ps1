
Invoke-WebRequest -Uri https://git.openprivacy.ca/openprivacy/buildfiles/raw/branch/master/tor/tor-0.4.8.9-win64.zip -OutFile tor.zip

if ((Get-FileHash tor.zip -Algorithm sha512).Hash -ne 'a1c90d9a2f82df5f3d973e260bbfec76e413417cfa276fb70d1668214e2e607cf65be86a25e30c6f7814261dc154a02568459945f31389ae6a22e03d6d0d3c4c' ) { Write-Error 'tor.zip sha512sum mismatch' }

Expand-Archive -Path tor.zip -DestinationPath Tor
