# NSIS Notes

## Images

Requires Windows 3 Compatible Bitmaps.

Can convert to the correct format with e.g. `mogrify -compress none -format bmp3 windows/nsis/cwtch_title.bmp
`