class SearchResult {
  String searchID;
  int conversationIdentifier;
  int messageIndex;
  SearchResult({required this.searchID, required this.conversationIdentifier, required this.messageIndex});
}
