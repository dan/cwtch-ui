import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../settings.dart';
import 'package:intl/intl.dart';

String redactedNick(BuildContext context, String handle, String nick) {
  var settings = Provider.of<Settings>(context, listen: false);
  if (settings.streamerMode) {
    if (handle == nick) {
      return handle.substring(0, 7) + "...";
    }
  }
  return nick;
}

String prettyDateString(BuildContext context, DateTime date) {
  var settings = Provider.of<Settings>(context, listen: false);
  // We want to display *Never* for epoch dates regardless of setting status
  if (date.millisecondsSinceEpoch == 0) {
    return AppLocalizations.of(context)!.conversationNotificationPolicyNever;
  }
  if (settings.streamerMode) {
    var now = DateTime.now();
    if (now.difference(date).abs().inDays > 1) {
      return AppLocalizations.of(context)!.xDaysAgo.replaceAll("\$days", now.difference(date).abs().inDays.toString());
    }
    if (now.difference(date).abs().inHours > 1) {
      return AppLocalizations.of(context)!.xHoursAgo.replaceAll("\$hours", now.difference(date).abs().inHours.toString());
    }
    if (now.difference(date).abs().inMinutes > 1) {
      return AppLocalizations.of(context)!.xMinutesAgo.replaceAll("\$minutes", now.difference(date).abs().inMinutes.toString());
    }
    // This updates too frequently and looks a little silly. Leaving here as documentation, just in case we want this for something else...
    // if (now.difference(date).abs().inSeconds > 1) {
    //   return AppLocalizations.of(context)!.xSecondsAgo.replaceAll("\$seconds", now.difference(date).abs().inSeconds.toString());
    // }
    return AppLocalizations.of(context)!.now;
  }
  // note: explicitly overriding the 'locale' here to force
  // a consistent YYYY-mm-dd HH::MM format.
  return DateFormat.yMd('en_CA').add_jm().format(date.toLocal());
}
