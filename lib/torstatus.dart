import 'package:flutter/material.dart';

class TorStatus extends ChangeNotifier {
  int progress;
  String status;
  bool connected;
  String version;

  TorStatus({this.connected = false, this.progress = 0, this.status = "", this.version = ""});

  /// Called by the event bus.
  handleUpdate(int newProgress, String newStatus) {
    if (progress == 100) {
      connected = true;
    } else {
      connected = false;
    }

    progress = newProgress;
    status = newStatus;
    if (newProgress != 100) {
      status = "$newProgress% - $newStatus";
    }

    notifyListeners();
  }

  updateVersion(String newVersion) {
    version = newVersion;
    notifyListeners();
  }
}
