#!/bin/sh

# Run from SRCROOT

cp libCwtch.x64.dylib build/macos/Build/Products/Release/Cwtch.app/Contents/Frameworks/
cp libCwtch.arm64.dylib build/macos/Build/Products/Release/Cwtch.app/Contents/Frameworks/
cp -r macos/Tor build/macos/Build/Products/Release/Cwtch.app/Contents/MacOS/



rm Cwtch.dmg
rm -r macos_dmg
mkdir macos_dmg
cp -r "build/macos/Build/Products/Release/Cwtch.app" macos_dmg/
# prep DMG with pregenerated components from GUI supportred run of create-dmg
cp macos/dmg/.DS_Store macos_dmg/
cp macos/dmg/.VolumeIcon.icns macos_dmg/
ln -s /Applications macos_dmg/Applications
hdiutil create -fs HFS+ -volname Cwtch -srcfolder macos_dmg Cwtch.dmg

# create-dmg requires GUI and is therefore less suited to automated builds, preserving here for
# manual runs to generate new .DS_Store and .VolumeIcon.icns for capture to the dmg dir for automated reuse
# https://github.com/create-dmg/create-dmg
#create-dmg \
#	--volname "Cwtch" \
#	--volicon "macos/cwtch.icns" \
#	--window-pos 200 120 \
#	--window-size 800 400 \
#	--icon-size 100 \
#	--icon "Cwtch.app" 200 190 \
#	--hide-extension "Cwtch.app" \
#	--app-drop-link 600 185 \
#	"Cwtch.dmg" \
#	macos_dmg
